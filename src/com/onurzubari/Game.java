package com.onurzubari;

import java.util.concurrent.ThreadLocalRandom;

public class Game {

    private int firstPlayerScore;
    private int secondPlayerScore;
    private int winner;
    private boolean doubleScore = false;
    private boolean tripleScore = false;
    private SportradarDB database;
    private int matchID;
    private int firstLegID;
    private int secondLegID;
    private int thirdLegID;
    private int currentLegID;

    public Game(int type, DartPlayer player1, DartPlayer player2, SportradarDB database){

        this.database = database;
        matchID = this.database.addMatch(player1.getPlayerID(), player2.getPlayerID());

        DartPlayer firstPlayer;
        DartPlayer secondPlayer;
        if (Math.random() < 0.5){
            firstPlayer = player1;
            secondPlayer = player2;
        }
        else{
            firstPlayer = player2;
            secondPlayer = player1;
        }

        int score;
        int total;

        for(int i = 0; i <3; i++){
            if(i == 0){
                firstLegID = database.addLeg();
                currentLegID = firstLegID;
            } else if(i == 1){
                secondLegID = database.addLeg();
                currentLegID = secondLegID;
            }else {
                thirdLegID = database.addLeg();
                currentLegID = thirdLegID;
            }
            winner = 0;
            if(type == 1)
                firstPlayerScore = secondPlayerScore = 501;
            else if(type == 2)
                firstPlayerScore = secondPlayerScore = 301;

            while (winner == 0){
                total = 0;
                for(int j = 0; j <3; j++){
                    score = throwDart(player1, currentLegID);

                    if((firstPlayerScore - score) >= 0 && (firstPlayerScore - score) != 1){
                        firstPlayerScore -= score;
                        total += score;
                        if(doubleScore){
                            System.out.println(firstPlayer.getPlayerName() + "'s score: " + firstPlayerScore + " (D)");
                        } else if(tripleScore) {
                            System.out.println(firstPlayer.getPlayerName() + "'s score: " + firstPlayerScore + " (T)");
                        } else{
                            System.out.println(firstPlayer.getPlayerName() + "'s score: " + firstPlayerScore);
                        }

                    } else{
                        System.out.println(firstPlayer.getPlayerName() + " busted.");
                        firstPlayerScore += total;
                        break;
                    }

                    if(firstPlayerScore == 0 && doubleScore){
                        winner = firstPlayer.getPlayerID();
                        System.out.println("Winner is " + firstPlayer.getPlayerName() + "!");
                        database.updateLegWinner(currentLegID, firstPlayer.getPlayerID());
                        break;
                    } else if(firstPlayerScore == 0 && !doubleScore){
                        System.out.println(firstPlayer.getPlayerName() + " busted.");
                        firstPlayerScore += total;
                        break;
                    }
                }

                total = 0;
                if(winner == 0){
                    for(int j = 0; j <3; j++){
                        score = throwDart(player2, currentLegID);

                        if((secondPlayerScore - score) >= 0 && (secondPlayerScore - score) != 1){
                            secondPlayerScore -= score;
                            total += score;
                            if(doubleScore){
                                System.out.println(secondPlayer.getPlayerName() + "'s score: " + secondPlayerScore + " (D)");
                            } else if(tripleScore) {
                                System.out.println(secondPlayer.getPlayerName() + "'s score: " + secondPlayerScore + " (T)");
                            } else{
                                System.out.println(secondPlayer.getPlayerName() + "'s score: " + secondPlayerScore);
                            }
                        } else{
                            System.out.println(secondPlayer.getPlayerName() + " busted.");
                            secondPlayerScore += total;
                            break;
                        }

                        if(secondPlayerScore == 0 && doubleScore){
                            winner = secondPlayer.getPlayerID();
                            System.out.println("Winner is " + secondPlayer.getPlayerName() + "!");
                            database.updateLegWinner(currentLegID, secondPlayer.getPlayerID());
                            break;
                        } else if(secondPlayerScore == 0 && !doubleScore){
                            System.out.println(secondPlayer.getPlayerName() + " busted.");
                            secondPlayerScore += total;
                            break;
                        }
                    }
                }
            }
        }
        database.updateLegAverage(firstLegID, database.getLegSpesificAverage(firstPlayer.getPlayerID(), firstLegID), database.getLegSpesificAverage(secondPlayer.getPlayerID(), firstLegID));
        database.updateLegAverage(secondLegID, database.getLegSpesificAverage(firstPlayer.getPlayerID(), secondLegID), database.getLegSpesificAverage(secondPlayer.getPlayerID(), secondLegID));
        database.updateLegAverage(thirdLegID, database.getLegSpesificAverage(firstPlayer.getPlayerID(), thirdLegID), database.getLegSpesificAverage(secondPlayer.getPlayerID(), thirdLegID));
        database.updateLegThrows(firstLegID, database.getLegThrowCount(firstPlayer.getPlayerID(), firstLegID), database.getLegThrowCount(secondPlayer.getPlayerID(), firstLegID));
        database.updateLegThrows(secondLegID, database.getLegThrowCount(firstPlayer.getPlayerID(), secondLegID), database.getLegThrowCount(secondPlayer.getPlayerID(), secondLegID));
        database.updateLegThrows(thirdLegID, database.getLegThrowCount(firstPlayer.getPlayerID(), thirdLegID), database.getLegThrowCount(secondPlayer.getPlayerID(), thirdLegID));
        database.updateMatchLegs(matchID, firstLegID, secondLegID, thirdLegID);

        database.updateMatchWinner(matchID, database.getLegWins(matchID, firstPlayer.getPlayerID(), secondPlayer.getPlayerID()));
        updatePlayersWinLose(firstPlayer);
        updatePlayersWinLose(secondPlayer);
        database.updatePlayerAverage(firstPlayer.getPlayerID());
        database.updatePlayerAverage(secondPlayer.getPlayerID());
    }

    private int throwDart(DartPlayer player, int legID){
        /*try
        {
            Thread.sleep(100);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }*/

        doubleScore = false;
        tripleScore = false;
        int score;
        score = ThreadLocalRandom.current().nextInt(1, 20 + 1);
        if (Math.random() < (double)player.getPlayerSkillLevel()/10){
            score *= 3;
            tripleScore = true;
        }
        else if(Math.random() < (double)player.getPlayerSkillLevel()/5){
            score *= 2;
            doubleScore = true;
        }
        //System.out.println("Dart: " + i+1 + " " + player.getPlayerName() + " scored: " + score);
        database.addDartScore(player.getPlayerID(), legID, matchID, score);
        return score;
    }

    private void updatePlayersWinLose(DartPlayer player){
        database.updatePlayer(player.getPlayerID(),database.getPlayerWinCount(player.getPlayerID()), database.getPlayerMatchCount(player.getPlayerID()) - database.getPlayerWinCount(player.getPlayerID()));
    }
}
