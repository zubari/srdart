package com.onurzubari;

public class DartPlayer {
    private int id;
    private String name;
    private int win;
    private int lose;
    private int average;
    private int skilllevel;

    public DartPlayer(int id, String name, int win, int lose, int average, int skilllevel){
        this.id = id;
        this.name = name;
        this.win = win;
        this.lose = lose;
        this.average = average;
        this.skilllevel = skilllevel;
    }

    public void setPlayerID(int id){
        this.id = id;
    }

    public int getPlayerID(){
        return id;
    }

    public void setPlayerName(String name){
        this.name = name;
    }

    public String getPlayerName(){
        return name;
    }

    public void setPlayerWin(int win){
        this.win = win;
    }

    public int getPlayerWin(){
        return win;
    }

    public void setPlayerLose(int lose){
        this.lose = lose;
    }

    public int getPlayerLose(){
        return lose;
    }

    public void setPlayerAverage(int average){
        this.average = average;
    }

    public int getPlayerAverage(){
        return average;
    }

    public void setPlayerSkillLevel(int skilllevel){
        this.skilllevel = skilllevel;
    }

    public int getPlayerSkillLevel(){
        return skilllevel;
    }
}
