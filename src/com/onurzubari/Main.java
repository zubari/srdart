package com.onurzubari;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        SportradarDB database = new SportradarDB();

        DartPlayer player1;
        DartPlayer player2;

        /*int id = player.getPlayerID();
        String name = player.getPlayerName();
        int rank = player.getPlayerRank();
        int win = player.getPlayerWin();
        int lose = player.getPlayerLose();
        int average = player.getPlayerAverage();
        int skilllevel = player.getPlayerSkillLevel();
        System.out.println("ID: " + id + " Name: " + name + " Rank: " + rank + " Win: " + win + " Lose: " + lose + " Average: " + average + " Skill Level: " + skilllevel);*/



        Scanner reader = new Scanner(System.in);

        System.out.println("----------WELCOME----------\n" +
                "Type '1' to show players.\n" +
                "Type '2' to insert a new player.\n" +
                "Type '3' to update a player.\n" +
                "Type '4' to delete a player.\n" +
                "Type '5' to arrange a match.\n" +
                "Type '6' to truncate the tables.");
        int selection = reader.nextInt();
        int playerID;
        switch (selection){
            case 1:
                database.showPlayers();
                break;
            case 2:
                System.out.println("Please enter the name of the player:");
                String name = reader.next();
                database.insertPlayer(name);
                break;
            case 3:
                System.out.println("Select a player to update: (Type ID number)");
                database.showPlayers();
                playerID = reader.nextInt();
                System.out.print("Please enter the new name of the player: ");
                String changedName = reader.next();
                database.updatePlayer(playerID, changedName);
                break;
            case 4:
                System.out.println("Select a player to delete: (Type ID number)");
                database.showPlayers();
                playerID = reader.nextInt();
                database.deletePlayer(playerID);
                break;
            case 5:
                System.out.println("Please select the game type: (Type '1' for 501, '2' for 301)");
                int gameType = reader.nextInt();
                System.out.println("Choose two players from the list below. (Type ID number)");
                database.showPlayers();
                int firstPlayerId = reader.nextInt();
                player1 = database.getPlayerById(firstPlayerId);
                int secondPlayerId = reader.nextInt();
                player2 = database.getPlayerById(secondPlayerId);
                System.out.println("Chosen players are: " + player1.getPlayerName() + ", " + player2.getPlayerName());
                Game game = new Game(gameType, player1, player2, database);
                break;
            case 6:
                database.clearTables();
                break;
        }
        reader.close();
    }
}
