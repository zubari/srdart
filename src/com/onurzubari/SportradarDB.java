package com.onurzubari;

import java.sql.*;
import java.util.Iterator;

public class SportradarDB {

    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;

    public SportradarDB(){

        try{
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sportradar?useUnicode=true&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=Turkey", "root", ""); //serverTimezone=Turkey is required for server's local timezone, change accordingly.
            statement = connection.createStatement();

        }catch (Exception exception){
            System.out.println("Connection exception: " + exception);
        }

    }

    public void insertPlayer(String name){
        try {

            String query = "INSERT INTO `dartplayers` (`id`, `name`, `win`, `lose`, `average`, `skilllevel`) VALUES (NULL, '" + name + "', NULL, NULL, NULL, NULL, '1')";
            statement.executeUpdate(query);

        }catch (Exception exception){
            System.out.println("Insert exception: " + exception);
        }
    }

    public void showPlayers(){
        try {

            String query = "SELECT * FROM dartplayers";
            resultSet = statement.executeQuery(query);
            System.out.println("Players recorded in the database:");

            while (resultSet.next()){
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                String win = resultSet.getString("win");
                String lose = resultSet.getString("lose");
                String average = resultSet.getString("average");
                String skilllevel = resultSet.getString("skilllevel");

                System.out.println("ID: " + id + " Name: " + name + " Win: " + win + " Lose: " + lose + " Average: " + average + " Skill level: " + skilllevel);
            }

        }catch (Exception exception){
            System.out.println("Select exception: " + exception);
        }
    }

    public int[] getAllPlayers(){
        int[] list, zero = new int[0];
        try {

            String query = "SELECT COUNT(1) FROM dartplayers";
            resultSet = statement.executeQuery(query);

            list = new int[resultSet.getInt(1)];

            String playerID = "SELECT id FROM dartplayers";
            resultSet = statement.executeQuery(playerID);
            int i = 0;
            while (resultSet.next()){
                list[i] = resultSet.getInt(1);
                i++;
            }

            return list;

        }catch (Exception exception){
            System.out.println("Select exception: " + exception);
        }
        return zero;
    }

    public DartPlayer getPlayerById(int id){

        String name, win, lose, average, skilllevel;
        name = win = lose = average = skilllevel = null;
        try {

            String query = "SELECT * FROM dartplayers WHERE id =" + id;
            resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                name = resultSet.getString("name");
                win = resultSet.getString("win");
                lose = resultSet.getString("lose");
                average = resultSet.getString("average");
                skilllevel = resultSet.getString("skilllevel");
            }

        }catch (Exception exception){
            System.out.println("Select exception: " + exception);
        }

        /*assert win != null : "Win is null.";
        assert lose != null : "Lose is null.";
        assert average != null : "Average is null.";*/

        win = (win == null) ? "0" : win;
        lose = (lose == null) ? "0" : lose;
        average = (average == null) ? "0" : average;
        skilllevel = (skilllevel == null) ? "1" : skilllevel;

        return new DartPlayer(id, name, Integer.parseInt(win), Integer.parseInt(lose), Integer.parseInt(average), Integer.parseInt(skilllevel));
    }

    public void updatePlayer(int id, String name){
        try {

            String query = "UPDATE dartplayers SET name ='"+name+"' WHERE id="+id;
            statement.executeUpdate(query);

        }catch (Exception exception){
            System.out.println("Update exception: " + exception);
        }
    }

    public void updatePlayer(int id, int win, int lose){
        try {

            String query = "UPDATE dartplayers SET win = '"+win+"', lose = '"+lose+"' WHERE id = " + id;
            statement.executeUpdate(query);

        }catch (Exception exception){
            System.out.println("Update exception: " + exception);
        }
    }

    public void updatePlayerAverage(int id){
        try {

            float average = getPlayerAverage(id);
            String query = "UPDATE dartplayers SET average = '"+average+"' WHERE id = " + id;
            statement.executeUpdate(query);

        }catch (Exception exception){
            System.out.println("Update exception: " + exception);
        }
    }

    public void deletePlayer(int id){
        try {

            String query = "DELETE FROM dartplayers WHERE id =" + id;
            statement.executeUpdate(query);

        }catch (Exception exception){
            System.out.println("Delete exception: " + exception);
        }
    }

    public void addDartScore(int id, int legid, int matchid, int score){
        try {

            String query = "INSERT INTO `throw` (`id`, `legid`, `playerid`, `matchid`, `score`) VALUES (NULL, '"+legid+"', '"+id+"', '"+matchid+"', '"+score+"')";
            statement.executeUpdate(query);

        }catch (Exception exception){
            System.out.println("Add throw exception: " + exception);
        }
    }

    public float getLegSpesificAverage(int playerID, int legID){
        try {

            String query = "SELECT score FROM throw WHERE playerid = " + playerID + " AND legid = " + legID;
            resultSet = statement.executeQuery(query);

            int count = 0, total = 0;
            while (resultSet.next()) {
                count++;
                total += resultSet.getInt(1);
            }
            return total/count;

        }catch (Exception exception){
            System.out.println("Leg average exception: " + exception);
        }

        return 0;
    }

    public int getLegWins(int matchID, int firstPlayerId, int secondPlayerId){
        try {

            String query = "SELECT `firstleg`, `secondleg`, `thirdleg` FROM `matches` WHERE id = " + matchID;
            resultSet = statement.executeQuery(query);


            int[] leg = new int[3];
            while (resultSet.next()) {
                leg[0] = resultSet.getInt(1);
                leg[1] = resultSet.getInt(2);
                leg[2] = resultSet.getInt(3);
            }

            int firstPlayerWins = 0;
            for (int legID : leg) {
                String legQuery = "SELECT `outcome` FROM `leg` WHERE id = " + legID;
                resultSet = statement.executeQuery(legQuery);

                while (resultSet.next()) {
                    if(resultSet.getInt(1) == firstPlayerId){
                        firstPlayerWins++;
                    }
                }
            }

            if (firstPlayerWins >= 2)
                return firstPlayerId;
            else
                return secondPlayerId;

        }catch (Exception exception){
            System.out.println("Leg wins exception: " + exception);
        }
        return 0;
    }

    public void updateMatchWinner(int matchID, int winnerID){
        try {

            String query = "UPDATE `matches` SET `outcome` = '"+winnerID+"' WHERE `matches`.`id` = " + matchID;
            statement.executeUpdate(query);

        }catch (Exception exception){
            System.out.println("Update winner exception: " + exception);
        }
    }

    public int getLegThrowCount(int playerID, int legID){
        try {

            String query = "SELECT COUNT(1) FROM throw WHERE playerid = " + playerID + " AND legid = " + legID;
            resultSet = statement.executeQuery(query);

            int count = 0;
            while (resultSet.next()) {
                count = resultSet.getInt(1);
            }
            return count;

        }catch (Exception exception){
            System.out.println("Leg average exception: " + exception);
        }
        return 0;
    }

    public int getPlayerWinCount(int playerID){
        try {
            String query = "SELECT COUNT(1) FROM matches WHERE (firstplayer = " + playerID + " OR secondplayer = " + playerID + ") AND outcome = " + playerID;
            resultSet = statement.executeQuery(query);

            int count = 0;
            while (resultSet.next()) {
                count = resultSet.getInt(1);
            }
            return count;

        }catch (Exception exception){
            System.out.println("Win count exception: " + exception);
        }
        return 0;
    }

    public int getPlayerMatchCount(int playerID){
        try {
            String query = "SELECT COUNT(1) FROM matches WHERE firstplayer = " + playerID + " OR secondplayer = " + playerID;
            resultSet = statement.executeQuery(query);

            int count = 0;
            while (resultSet.next()) {
                count = resultSet.getInt(1);
            }
            return count;

        }catch (Exception exception){
            System.out.println("Win count exception: " + exception);
        }
        return 0;
    }

    public int getPlayerAverage(int playerID){
        try {
            String query = "SELECT score FROM throw WHERE playerid = " + playerID;
            resultSet = statement.executeQuery(query);

            int count = 0, total = 0;
            while (resultSet.next()) {
                count++;
                total += resultSet.getInt(1);
            }
            return total/count;

        }catch (Exception exception){
            System.out.println("Player average exception: " + exception);
        }
        return 0;
    }

    public int addMatch(int firstPlayerID, int secondPlayerID){
        try {

            String query = "INSERT INTO `matches` (`id`, `firstplayer`, `secondplayer`) VALUES (NULL, '"+firstPlayerID+"', '"+secondPlayerID+"')";
            PreparedStatement preparedStatement = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            return resultSet.getInt(1);

        }catch (Exception exception){
            System.out.println("Add match exception: " + exception);
        }
        return 0;
    }

    public void updateMatchLegs(int matchid, int firstLeg, int secondLeg, int thirdLeg){
        try {

            String query = "UPDATE `matches` SET `firstleg` = '"+firstLeg+"', `secondleg` = '"+secondLeg+"', `thirdleg` = '"+thirdLeg+"' WHERE `matches`.`id` = "+ matchid;
            statement.executeUpdate(query);

        }catch (Exception exception){
            System.out.println("Update exception: " + exception);
        }
    }

    public int addLeg(){
        try {

            String query = "INSERT INTO `leg` (`id`) VALUES (NULL)";
            PreparedStatement preparedStatement = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            return resultSet.getInt(1);

        }catch (Exception exception){
            System.out.println("Add leg exception: " + exception);
        }

        return 0;
    }

    public void updateLegAverage(int legID, float firstPlayerAverage, float secondPlayerAverage){
        try {

            String query = "UPDATE `leg` SET `firstplayeraverage` = '"+firstPlayerAverage+"', `secondplayeraverage` = '"+secondPlayerAverage+"' WHERE `leg`.`id` = " + legID;
            statement.executeUpdate(query);

        }catch (Exception exception){
            System.out.println("Update leg average exception: " + exception);
        }

    }

    public void updateLegThrows(int legID, int firstPlayerThrow, float secondPlayerThrow){
        try {

            String query = "UPDATE `leg` SET `firstplayerthrows` = '"+firstPlayerThrow+"', `secondplayerthrows` = '"+secondPlayerThrow+"' WHERE `leg`.`id` = " + legID;
            statement.executeUpdate(query);

        }catch (Exception exception){
            System.out.println("Update leg throws exception: " + exception);
        }

    }

    public void updateLegWinner(int legID, int playerID){
        try {

            String query = "UPDATE `leg` SET `outcome` = '"+playerID+"' WHERE `leg`.`id` = " + legID;
            statement.executeUpdate(query);

        }catch (Exception exception){
            System.out.println("Update leg winner exception: " + exception);
        }

    }

    public void clearTables(){
        try {

            String[] truncatedTables = new String[] { "throw", "leg", "matches"};

            String disableConstraints = "SET FOREIGN_KEY_CHECKS = 0;";
            statement.executeUpdate(disableConstraints);
            for (String table : truncatedTables) {
                String query = "TRUNCATE `"+table+"`";
                statement.executeUpdate(query);
            }
            String enableConstraints = "SET FOREIGN_KEY_CHECKS = 1;";
            statement.executeUpdate(enableConstraints);

        }catch (Exception exception){
            System.out.println("Truncate exception: " + exception);
        }
    }
}
